#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include "io.h"
#include <stdint.h>
#include <stdio.h>

enum read_status read_bmp(char* filename, struct image** img);

enum write_status write_bmp(char* filename, struct image* img);

#endif //IMAGE_TRANSFORMER_BMP_H
