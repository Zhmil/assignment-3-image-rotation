//
// Created by Emil Askarov on 03.01.2024.
//

#ifndef IMAGE_TRANSFORMER_IO_H
#define IMAGE_TRANSFORMER_IO_H

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_ERROR,
//    READ_INVALID_SIGNATURE,
//    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_OPEN_ERROR
    /* коды других ошибок  */
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_OPEN_ERROR
    /* коды других ошибок  */
};

void print_read_status(enum read_status status);

void print_write_status(enum write_status status);

#endif //IMAGE_TRANSFORMER_IO_H
