#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image* transpose(struct image* src);

struct image* rotate(struct image* src, int16_t degree);

void destroy_image(struct image* src);

#endif //IMAGE_TRANSFORMER_IMAGE_H
