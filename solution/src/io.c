#include "io.h"
#include <stdio.h>

static const char* const READ_STATUS_DESCRIPTION[] = {
        [READ_OK] = "Successful reading",
        [READ_ERROR] = "Error occurred while reading",
        [READ_INVALID_HEADER] = "Invalid header",
        [READ_OPEN_ERROR] = "Failed to open file to read",
};

static const char* const WRITE_STATUS_DESCRIPTION[] = {
        [WRITE_OK] = "Successful writing",
        [WRITE_ERROR] = "Error occurred while writing",
        [WRITE_OPEN_ERROR] = "Failed to open file to write",
};

void print_read_status(enum read_status status) {
    printf("%s", READ_STATUS_DESCRIPTION[status]);
}

void print_write_status(enum write_status status) {
    printf("%s", WRITE_STATUS_DESCRIPTION[status]);
}
