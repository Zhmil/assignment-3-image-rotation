#include "bmp.h"
#include "image.h"
#include "io.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BYTE 8
#define DPI 220
#define INCHES_PER_METER 39
#define TYPE 19778
#define RESERVED 0
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define CLR_USED 0
#define CLR_IMPORTANT 0
#define PADDING_ALIGNMENT 4


struct __attribute__((packed)) bmp_file_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
};

struct __attribute__((packed)) bmp_info_header {
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct __attribute__((packed)) bmp_header {
    struct bmp_file_header file;
    struct bmp_info_header info;
};

static enum read_status read_header(FILE *in, struct bmp_header* header) {
    if (fseek(in, 0, SEEK_SET) != 0) {
        return READ_ERROR;
    }
    *header = (struct bmp_header) {0};
    if (fread(header, 1, sizeof(struct bmp_header), in) != sizeof(struct bmp_header)) {
        return READ_ERROR;
    }
    return READ_OK;
}

static size_t size(const struct bmp_header header) {
    return header.info.biHeight * header.info.biWidth * (header.info.biBitCount / BYTE);
}

static uint8_t get_padding(const size_t row_size) {
    return (row_size % PADDING_ALIGNMENT) ? PADDING_ALIGNMENT - row_size % PADDING_ALIGNMENT : 0;
}

static uint8_t* remove_padding(const uint8_t* const data, const struct bmp_header header) {
    uint8_t* filtered_data = malloc(size(header));
    if (filtered_data == NULL) {
        return NULL;
    }
    const size_t row_size = header.info.biWidth * (header.info.biBitCount / BYTE);
    const uint8_t padding = get_padding(row_size);

    for (size_t row = 0; row < header.info.biHeight; row++) {
        for (size_t col = 0; col < row_size; col++) {
            filtered_data[row * row_size + col] = data[row * (row_size + padding) + col];
        }
    }

    return filtered_data;
}

static enum read_status read_image(FILE* in, const struct bmp_header header, struct pixel** data) {
    if (fseek(in, header.file.bOffBits, SEEK_SET) != 0) {
        return READ_ERROR;
    }
    uint8_t* raw_data = malloc(header.info.biSizeImage);
    if (raw_data == NULL) {
        return READ_ERROR;
    }
    if (fread(raw_data, 1, header.info.biSizeImage, in) != header.info.biSizeImage) {
        return READ_ERROR;
    }
    *data = (struct pixel*) remove_padding(raw_data, header);
    free(raw_data);
    if (*data == NULL) {
        return READ_ERROR;
    }
    return READ_OK;
}

static uint8_t* add_padding(const uint8_t* data, const struct bmp_header header) {
    const size_t row_size = header.info.biWidth * (header.info.biBitCount / BYTE);
    const uint8_t padding = get_padding(row_size);
    uint8_t* filtered_data = malloc((row_size + padding) * header.info.biHeight);
    if (filtered_data == NULL) {
        return NULL;
    }
    for (size_t row = 0; row < header.info.biHeight; row++) {
        for (size_t col = 0; col < row_size + padding; col++) {
            filtered_data[row * (row_size + padding) + col] = (col >= row_size) ? 0 : data[row * row_size + col];
        }
    }
    return filtered_data;
}



static enum write_status write_header(FILE* out, const struct bmp_header header) {
    size_t n = fwrite(&header, 1, sizeof(struct bmp_header), out);
    return (n == sizeof(struct bmp_header)) ? WRITE_OK : WRITE_ERROR;
}

static enum write_status write_image(FILE* out, const struct bmp_header header, struct pixel* image) {
    uint8_t* raw_image = add_padding((uint8_t*) image, header);
    if (raw_image == NULL) {
        return WRITE_ERROR;
    }
    size_t n = fwrite(raw_image, 1, header.info.biSizeImage, out);
    free(raw_image);
    return (n == header.info.biSizeImage) ? WRITE_OK : WRITE_ERROR;
}

static enum read_status from_bmp(FILE* in, struct image** img) {
    enum read_status status;
    struct bmp_header header;
    struct pixel* data;

    status = read_header(in, &header);
    if (status != READ_OK) {
        return status;
    }
    if (header.file.bfType != TYPE) {
        return READ_INVALID_HEADER;
    }
    status = read_image(in, header, &data);
    if (status != READ_OK) {
        return status;
    }
    *img = malloc(sizeof(struct image));
    **img = (struct image) { .height = header.info.biHeight, .width = header.info.biWidth, .data = data };
    return READ_OK;
}


enum read_status read_bmp(char* filename, struct image** img) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        return READ_OPEN_ERROR;
    }
    enum read_status status = from_bmp(file, img);
    fclose(file);
    return status;
}

static struct bmp_header image_to_bmp_header(const struct image* img) {
    const size_t row_size = img->width * sizeof(struct pixel);
    const uint8_t padding = get_padding(row_size);
    const uint32_t imageSize = (row_size + padding) * img->height;
    return (struct bmp_header) {
            .file = {
                    .bfType = TYPE,
                    .bfileSize = imageSize + sizeof(struct  bmp_header),
                    .bfReserved = RESERVED,
                    .bOffBits = sizeof(struct bmp_header),
            },
            .info = {
                    .biSize = sizeof(struct bmp_info_header),
                    .biWidth = img->width,
                    .biHeight = img->height,
                    .biPlanes = PLANES,
                    .biBitCount = BIT_COUNT,
                    .biCompression = COMPRESSION,
                    .biSizeImage = imageSize,
                    .biXPelsPerMeter = DPI * INCHES_PER_METER,
                    .biYPelsPerMeter = DPI * INCHES_PER_METER,
                    .biClrUsed = CLR_USED,
                    .biClrImportant = CLR_IMPORTANT
            }
    };
}

static enum write_status to_bmp(FILE *out, struct image* const img) {
    const struct bmp_header header = image_to_bmp_header(img);
    enum write_status status;
    status = write_header(out, header);
    if (status != WRITE_OK) {
        return status;
    }
    status = write_image(out, header, img->data);
    if (status != WRITE_OK) {
        return status;
    }
    return WRITE_OK;
}

enum write_status write_bmp(char* filename, struct image* const img) {
    FILE* file = fopen(filename, "wb");
    if (file == NULL) {
        return WRITE_OPEN_ERROR;
    }
    enum write_status status = to_bmp(file, img);
    fclose(file);
    return status;
}
