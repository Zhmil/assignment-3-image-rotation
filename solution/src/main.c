#include "bmp.h"
#include "io.h"
#include <inttypes.h>
#include <stdio.h>

#define INVALID_ARGS_ERROR 100
#define FAILED_TO_ROTATE_ERROR 101
#define ARG_NUMBER 4

int main( int argc, char** argv ) {
    if (argc != ARG_NUMBER) {
        printf("Invalid argument number");
        return INVALID_ARGS_ERROR;
    }
    enum read_status read_status;
    enum write_status write_status;
    struct image* img;
    struct image* rotated_img;
    read_status = read_bmp(argv[1], &img);
    print_read_status(read_status);
    if (read_status != READ_OK) {
        return read_status;
    }
    printf("\n");
    int16_t degree;
    sscanf(argv[3], "%" SCNd16, &degree);
    rotated_img = rotate(img, degree);
    if (rotated_img == NULL) {
        printf("Failed to rotate image");
        return FAILED_TO_ROTATE_ERROR;
    }
    write_status = write_bmp(argv[2], rotated_img);
    print_write_status(write_status);
    if (write_status != WRITE_OK) {
        return write_status;
    }
    destroy_image(img);
    destroy_image(rotated_img);
    return 0;
}
