#include "image.h"
#include <printf.h>
#include <stdint.h>
#include <stdlib.h>

void destroy_image(struct image* const src) {
    free(src->data);
    free(src);
}

struct image* create_image(uint64_t width, uint64_t height) {
    size_t size = height * width * sizeof(struct pixel);
    struct pixel* data = malloc(size);
    if (data == NULL) {
        return NULL;
    }
    struct image* image = malloc(sizeof(struct image));
    if (image == NULL) {
        free(data);
        return NULL;
    }
    image->data = data;
    image->width = width;
    image->height = height;
    return image;

}

struct image* copy_image(struct image* const src) {
    struct image* image = create_image(src->width, src->height);
    if (image == NULL) {
        return NULL;
    }
    for (size_t i = 0; i < src->height * src->width; i++) {
        image->data[i] = src->data[i];
    }
    return image;
}

struct image* transpose(struct image* const src) {
    struct image* image = create_image(src->height, src->width);
    if (image == NULL) {
        return NULL;
    }
    for (size_t row = 0; row < src->height; row++) {
        for (size_t col = 0; col < src->width; col++) {
            size_t new_col = row;
            size_t new_row = src->width - col - 1;
            size_t new_i = new_row * src->height + new_col;
            size_t i = row * src->width + col;
            image->data[new_i] = src->data[i];
        }
    }
    return image;
}

struct image* rotate(struct image* const src, int16_t degree) {
    uint8_t n = (360 + degree) / 90;
    struct image* dst = copy_image(src);
    if (dst == NULL) {
        return NULL;
    }
    struct image* prev_dst = dst;
    for (uint8_t i = 0; i < n; i++) {
        dst = transpose(dst);
        destroy_image(prev_dst);
        prev_dst = dst;
        if (dst == NULL) {
            return NULL;
        }
    }
    return dst;
}
